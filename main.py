import platform     # platform.system() to get OS
import subprocess   # subprocess.call() to run a system command
from datetime import datetime      #datetime.now()


class Host:
    def __init__ (self, name, url):
        self.name = name
        self.url = url

    def __str__(self):
        return f'{self.name} {self.url}'


'''subprocess.run() doesn't capture stdout or stderr, so it will merely run the command as normal.  stdout/err must be
piped to suppress their output

os.system()/os.popen()  is deprecated and subject to shell injection
subprocess.call() returns the return value as its output, but if using shell=True it poses the same shell injection
risks. using shell=False(The default) prevents this injection.  If we want not just the return value but the stdout
we can use subprocess.check_output()
subprocess.run() is recommended since python 3.5, returns an object with the input command and return code, get stdout
by using capture_output=True, output.returncode() and output.stdout() for code and output
https://hackernoon.com/calling-shell-commands-from-python-ossystem-vs-subprocess-mc253z4f'''
def ping1():
    host = input("Please enter the IP address or domain you wish to ping:\n")
    param = '-n' if platform.system().lower() == 'windows' else '-c'
    command = ['ping', param, '1', host]
    return subprocess.run(command, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL).returncode == 0
    # os.popen() is a part of Python2
def ping2(host):
    param = '-n' if platform.system().lower() == 'windows' else '-c'
    command = ['ping', param, '1', host]
    # bool = print(subprocess.run(command, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL).returncode == 0)
    return subprocess.run(command, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL).returncode == 0
    # os.popen() is a part of Python2


def play():
    print(f'{__name__}')
    test = 10
    print(test)
    if test > 5:
        test = 2*test
    print(test)
    testlist = ["apple", "peach", "cherry"]
    print(testlist[1])
    print(len(testlist))
    for x in testlist:
        x = x + " pie"
        print(x)
    print(list)
    i = 0
    while i < len(testlist):
        print(type(x))
        list[i] = testlist[i] + " pie"
        i += 1
    print(testlist)

    print(platform.system())


def get_hosts():
    host1 = {'name': 'FreeNAS', 'address': '10.10.10.198'}
    host1.append
    print(host1['name'] + ' ' + host1['address'])
    print(host1)
    print(host1.items())
    for key in host1.items():
        print(key)


def setup_hosts():
    test_host = Host('Router', '10.10.10.1')
    test_host2 = Host('FreeNAS', '10.10.10.198')
    test_host3 = Host('XCP-Ng', '10.10.10.197')
    test_host4 = Host('badhost', '10.10.11.1')
    host_list = [test_host, test_host2, test_host3, test_host4]
    return host_list


if __name__ == '__main__':  # Only runs when this module is run directly, not called as a library
    # https://stackoverflow.com/questions/419163/what-does-if-name-main-do
    # play()
    # print(ping())
    # get_hosts()
    print('main')
    # print(*hostList, sep='\n')
    ping2('10.10.10.1')
    host_list = setup_hosts()
    while True:
        for x in range(len(host_list)):
            # print(host_list[x].name + ' \t' + host_list[x].url + ' \t' + host_list[x].url + ' \t' + datetime.now().strftime("%H:%M:%S"))
            row = "{:20}{:16}{:9}{:6}"
            col1 = host_list[x].name
            col2 = host_list[x].url
            col3 = datetime.now().strftime("%H:%M:%S")
            col4 = format(ping2(host_list[x].url))
            print(row.format(col1, col2, col3, col4))
